# Estructura de datos

Un párrafo lorem ipsum dolor sit amet, consectetur adipiscing elit.
Integer tristique sit amet est eu vehicula. Vestibulum mollis pharetra
ultrices. Pellentesque tincidunt vestibulum elit, quis commodo arcu
imperdiet nec. Etiam ut sapien in erat porta eleifend ut a nunc.
Nulla facilisi. Maecenas eget nisl nec sem rutrum gravida.
Morbi euismod velit ultrices aliquam malesuada. 

## Listas ligadas

Otro párrafo lorem ipsum dolor sit amet, consectetur adipiscing elit.
Integer tristique sit amet est eu vehicula. Vestibulum mollis pharetra
ultrices. Pellentesque tincidunt vestibulum elit, quis commodo arcu
imperdiet nec. Etiam ut sapien in erat porta eleifend ut a nunc.
Nulla facilisi. Maecenas eget nisl nec sem rutrum gravida.
Morbi euismod velit ultrices aliquam malesuada. 

## Pilas y colas

Una lista sin enumeración:

- Apilar.
- Desapilar.
- Tope.
- Elemento.

Una lista con enumeración:

1. Apilar.
2. Desapilar.
3. Tope.
4. Elemento.

## Programacion en Java

Código fuente:

    class HolaMundo {
        public static void main(String[] args) {
            System.out.println("¡Hola, Mundo!"); 
        }
    }

Imagen en formato JPG:

![Una imagen](imagenes/una-imagen.jpg)

Un enlace:

[JimenezLeonAlonsoRaziel @ GitLab](https://gitlab.com/JimenezLeonAlonsoRaziel)